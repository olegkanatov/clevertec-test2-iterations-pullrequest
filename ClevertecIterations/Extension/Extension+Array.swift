//
//  Extension+Array.swift
//  ClevertecIterations
//
//  Created by Oleg Kanatov on 14.02.22.
//

extension Array {
    init(repeating: [Element], count: Int) {
        self.init([[Element]](repeating: repeating, count: count).flatMap{ $0 })
    }
}
