//
//  View.swift
//  ClevertecIterations
//
//  Created by Oleg Kanatov on 13.02.22.
//

import UIKit

class View: UIView {
    
    private lazy var editButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Edit", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 20)
        button.addTarget(self, action: #selector(editButtonTap), for: .touchUpInside)
        return button
    }()
    
    lazy var tableView = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupConstraints() {
        editButton.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(editButton)
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            editButton.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            editButton.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            editButton.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            editButton.heightAnchor.constraint(equalToConstant: 50),
            
            tableView.topAnchor.constraint(equalTo: editButton.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    @objc private func editButtonTap() {
        
        if tableView.isEditing == false {
            tableView.isEditing = true
        } else {
            tableView.isEditing = false
        }
    }
}
